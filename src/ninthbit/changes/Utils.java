package ninthbit.changes;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.servlet.http.HttpServletRequest;

public class Utils {

	public static String getRequest(String requestURL) throws IOException {
		URL url = new URL(requestURL); 
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();           
		connection.setDoOutput(true);
		connection.setDoInput(true);
		connection.setInstanceFollowRedirects(false); 
		connection.setRequestMethod("GET"); 
		connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded"); 
		connection.setRequestProperty("charset", "utf-8");
		connection.setUseCaches (false);
		
		byte[] buffer = new byte[100000]; // 100KB is more than enough
		int bytesRead = connection.getInputStream().read(buffer);
		String page = new String(buffer, 0, bytesRead, "UTF-8");
		
		connection.disconnect();
		
		return page;
	}
	
	public static String postRequest(String requestURL,String params) throws IOException {
		URL url = new URL(requestURL); 
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();           
		connection.setDoOutput(true);
		connection.setDoInput(true);
		connection.setInstanceFollowRedirects(false); 
		connection.setRequestMethod("POST"); 
		connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded"); 
		connection.setRequestProperty("charset", "utf-8");
		connection.setRequestProperty("Content-Length", "" + Integer.toString(params.getBytes().length));
		connection.setUseCaches (false);

		DataOutputStream wr = new DataOutputStream(connection.getOutputStream ());
		wr.writeBytes(params);
		wr.flush();
		wr.close();
		
		byte[] buffer = new byte[100000]; // 100KB is more than enough
		int bytesRead = connection.getInputStream().read(buffer);
		String page = new String(buffer, 0, bytesRead, "UTF-8");
		
		connection.disconnect();
		
		return page;
	}
	
	public static ISchool getISchool(HttpServletRequest req) throws IOException {
		String schoolID = req.getPathInfo().split("/")[1];
		ISchool school;
		try {
			school = (ISchool) Class.forName(schoolID).newInstance();
		} catch (InstantiationException | IllegalAccessException| ClassNotFoundException e) {
			throw new IOException(e);
		}
		return school;
	}
	
	public static String getClassID(HttpServletRequest req) throws IOException {
		return req.getPathInfo().split("/")[2];
	}
	
}
