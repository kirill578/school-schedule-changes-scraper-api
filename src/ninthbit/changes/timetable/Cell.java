package ninthbit.changes.timetable;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.google.appengine.labs.repackaged.org.json.JSONArray;
import com.google.appengine.labs.repackaged.org.json.JSONException;
import com.google.appengine.labs.repackaged.org.json.JSONObject;

public class Cell {
	
	public int mLessonNumber;
	public String lessonTime;
	public ArrayList<Lesson> mLessons;
	
	public Cell(int lessonNumber, String lessonTime, ArrayList<Lesson> lessons) {
		this.mLessonNumber = lessonNumber;
		this.lessonTime = lessonTime;
		this.mLessons = lessons;
	}
	
	public JSONObject toJSON() throws JSONException {
		JSONArray lessons = new JSONArray();
		for (Lesson lesson : mLessons) {
			lessons.put(lesson.toJSON());
		}
		
		JSONObject cell = new JSONObject();
		cell.put("time", lessonTime);
		cell.put("lessons", lessons);
		
		return cell;
	}
	
}