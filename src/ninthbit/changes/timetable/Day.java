package ninthbit.changes.timetable;

import java.util.ArrayList;

import com.google.appengine.labs.repackaged.org.json.JSONArray;
import com.google.appengine.labs.repackaged.org.json.JSONException;
import com.google.appengine.labs.repackaged.org.json.JSONObject;

public class Day {

	String name;
	ArrayList<Cell> cells;
	
	public Day(String name, ArrayList<Cell> cells) {
		this.name = name;
		this.cells = cells;
	}
	
	public Day(String name) {
		this.name = name;
		this.cells = new ArrayList<Cell>();
	}
	
	public void addCell(Cell cell) {
		cells.add(cell);
	}

	public JSONObject toJSON() throws JSONException {
		JSONObject jo = new JSONObject();
		jo.put("name", name);
		
		JSONArray ja = new JSONArray();
		for (Cell cell : cells) 
			ja.put(cell.toJSON());
		
		jo.put("cells", ja);
		
		return jo;
	}
	
}
