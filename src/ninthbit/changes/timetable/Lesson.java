package ninthbit.changes.timetable;

import com.google.appengine.labs.repackaged.org.json.JSONException;
import com.google.appengine.labs.repackaged.org.json.JSONObject;

public class Lesson {
	public String name;
	public String teacher;
	
	public Lesson(String name, String teacher) {
		this.name = name;
		this.teacher = teacher;
	}

	public JSONObject toJSON() throws JSONException {
		JSONObject o = new JSONObject();
		o.put("name", name);
		o.put("teacher", teacher);
		return o;
	}
}