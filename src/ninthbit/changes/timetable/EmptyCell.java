package ninthbit.changes.timetable;

import java.util.ArrayList;

public class EmptyCell extends Cell {

	public EmptyCell(int lessonNumber, String lessonTime) {
		super(lessonNumber, lessonTime, new ArrayList<Lesson>());
	}

}
