package ninthbit.changes.endpoint;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ninthbit.changes.Utils;
import ninthbit.changes.timetable.Day;

import com.google.appengine.labs.repackaged.org.json.JSONArray;
import com.google.appengine.labs.repackaged.org.json.JSONException;

public class SchoolTimeTableEndPoint extends HttpServlet {
	
	private static final long serialVersionUID = 1L;

	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		resp.setContentType("application/json");
		resp.setCharacterEncoding("UTF-8");
		
		List<Day> list = Utils.getISchool(req).getTimeTable(Utils.getClassID(req));
		JSONArray ja = new JSONArray();
		for (Day day : list) {
			try {
				ja.put(day.toJSON());
			} catch (JSONException e) {
				throw new IOException(e);
			}
		}
		
		resp.getWriter().println(ja);
	}
	
	
	
	


}
