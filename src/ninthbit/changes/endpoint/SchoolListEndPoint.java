package ninthbit.changes.endpoint;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ninthbit.changes.ISchool;
import ninthbit.changes.schools.Alliance;
import ninthbit.changes.schools.Alon;
import ninthbit.changes.schools.AmalAlefPetah;
import ninthbit.changes.schools.DaganAndShaked;
import ninthbit.changes.schools.EhanAHamPetahTikva;
import ninthbit.changes.schools.GinsburgYavne;
import ninthbit.changes.schools.Hadera;
import ninthbit.changes.schools.Haderahb;
import ninthbit.changes.schools.Hertzog;
import ninthbit.changes.schools.Holtz;
import ninthbit.changes.schools.Ishalom;
import ninthbit.changes.schools.Makif11;
import ninthbit.changes.schools.MekifHetRishon;
import ninthbit.changes.schools.MekifKeriatHaim;
import ninthbit.changes.schools.MekifTetNeotAshelim;
import ninthbit.changes.schools.Mosenson;
import ninthbit.changes.schools.RabinBerSheva;
import ninthbit.changes.schools.Ramlea;
import ninthbit.changes.schools.Shazar;
import ninthbit.changes.schools.Shazarhb;
import ninthbit.changes.schools.ShinShinDentziger;
import ninthbit.changes.schools.Shnegev;
import ninthbit.changes.schools.Shoham;
import ninthbit.changes.schools.TihonIalmaIalin;
import ninthbit.changes.schools.Vavbs;
import ninthbit.changes.schools.Yavne;
import ninthbit.changes.schools.Zeitlin;

import com.google.appengine.labs.repackaged.org.json.JSONArray;
import com.google.appengine.labs.repackaged.org.json.JSONException;
import com.google.appengine.labs.repackaged.org.json.JSONObject;

public class SchoolListEndPoint extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	
	public static List<ISchool> schools = new ArrayList<ISchool>();
	static {
		schools.add(new Holtz());
		schools.add(new GinsburgYavne());
		schools.add(new MekifTetNeotAshelim());
		schools.add(new MekifKeriatHaim());
		schools.add(new AmalAlefPetah());
		
		schools.add(new MekifHetRishon());
		schools.add(new ShinShinDentziger());
		schools.add(new TihonIalmaIalin());
		schools.add(new EhanAHamPetahTikva());
		
		schools.add(new Alliance());
		schools.add(new Alon());
		schools.add(new DaganAndShaked());
		schools.add(new Hadera());
		schools.add(new Haderahb());
		schools.add(new Hertzog());
		schools.add(new Ishalom());
		schools.add(new Makif11());
		schools.add(new Mosenson());
		schools.add(new RabinBerSheva());
		schools.add(new Ramlea());
		schools.add(new Shazar());
		schools.add(new Shazarhb());
		schools.add(new Shnegev());
		schools.add(new Shoham());
		schools.add(new Vavbs());
		schools.add(new Yavne());
		schools.add(new Zeitlin());
	}
	
	
	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		resp.setContentType("application/json");
		resp.setCharacterEncoding("UTF-8");
		
		JSONArray jsonArray= new JSONArray();
		for (ISchool school : schools) {
			JSONObject jos = new JSONObject();
			try {
				jos.put("name", school.getName());
				jos.put("id", school.getClass().getCanonicalName());
			} catch (JSONException e) {
				// unlikly to get here
				throw new IOException(e);
			}
			jsonArray.put(jos);
		}
		
		
		resp.getWriter().print(jsonArray);
		
	}

}
