package ninthbit.changes.endpoint;

import java.io.IOException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ninthbit.changes.Utils;

import com.google.appengine.labs.repackaged.org.json.JSONException;

public class SchoolChangesEndPoint extends HttpServlet {
	
	private static final long serialVersionUID = 1L;

	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		resp.setContentType("application/json");
		resp.setCharacterEncoding("UTF-8");
		try {
			resp.getWriter().print(Utils.getISchool(req).getChanges(Utils.getClassID(req)).toJSON());
		} catch (JSONException e) {
			throw new IOException(e);
		}
		
	}

}
