package ninthbit.changes.endpoint;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ninthbit.changes.ISchool;
import ninthbit.changes.SchoolClass;
import ninthbit.changes.StringEscapeUtils;

import com.google.appengine.labs.repackaged.org.json.JSONArray;
import com.google.appengine.labs.repackaged.org.json.JSONException;
import com.google.appengine.labs.repackaged.org.json.JSONObject;

public class SchoolClassesEndPoint extends HttpServlet {
	
	private static final long serialVersionUID = 1L;

	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		resp.setContentType("application/json");
		resp.setCharacterEncoding("UTF-8");
		
		List<SchoolClass> list = getISchool(req).getClasses();
		JSONArray array = new JSONArray();
		for (SchoolClass schoolClass : list) {
			JSONObject jo = new JSONObject();
			try {
				jo.put("name", schoolClass.getName());
				jo.put("ID", schoolClass.getID());
			} catch (JSONException e) {
				throw new IOException(e);
			}
			array.put(jo);
		}
		
		resp.getWriter().print(array);
		
	}

	private ISchool getISchool(HttpServletRequest req) throws IOException {
		String schoolID = req.getPathInfo().replace("/", "");
		ISchool school;
		try {
			school = (ISchool) Class.forName(schoolID).newInstance();
		} catch (InstantiationException | IllegalAccessException| ClassNotFoundException e) {
			throw new IOException(e);
		}
		return school;
	}

}
