package ninthbit.changes.schools;

import ninthbit.changes.schools.helper.AbstractSimpleIscool;


public class Holtz extends AbstractSimpleIscool {

	@Override
	public String getName() {
		return "הולץ";
	}

	@Override
	public String getURL() {
		return "http://holtz.iscool.co.il/%D7%93%D7%A3%D7%91%D7%99%D7%AA/tabid/1412/language/en-US/Default.aspx";
	}

	@Override
	public String getStateView() {
		return "/wEPDwUIMjU3MTQzOTcPZBYGZg8WAh4EVGV4dAU+PCFET0NUWVBFIEhUTUwgUFVCTElDICItLy9XM0MvL0RURCBIVE1MIDQuMCBUcmFuc2l0aW9uYWwvL0VOIj5kAgEPZBYMAgEPFgIeB1Zpc2libGVoZAICDxYCHgdjb250ZW50BT7XkdeZ16og15TXodek16gg15TXqNeRINeq15fXldee15kg15TXldec16Ug15fXmdecINeU15DXldeV15nXqGQCAw8WAh8CBU3XkdeZ16og15TXodek16gg15TXqNeRINeq15fXldee15kg15TXldec16Ug15fXmdecINeU15DXldeV15nXqCxEb3ROZXROdWtlLEROTmQCBA8WAh8CBSDXm9ecINeU15bXm9eV15nXldeqINep157Xldeo15XXqmQCBQ8WAh8CBQtEb3ROZXROdWtlIGQCBg8WAh8CBT7XkdeZ16og15TXodek16gg15TXqNeRINeq15fXldee15kg15TXldec16Ug15fXmdecINeU15DXldeV15nXqGQCAg9kFgJmD2QWAgIED2QWAmYPZBYSAgIPZBYCZg8PFgYeCENzc0NsYXNzBQtza2luY29sdHJvbB4EXyFTQgICHwFoZGQCAw9kFgJmDw8WBh8DBQtza2luY29sdHJvbB8ABQVMb2dpbh8EAgJkZAIFDxYCHgVjbGFzcwUUdG9wcGFuZSBETk5FbXB0eVBhbmVkAgYPFgIfBQUZdG9wcmlnaHRwYW5lIEROTkVtcHR5UGFuZWQCBw8WAh8FBRh0b3BsZWZ0cGFuZSBETk5FbXB0eVBhbmVkAggPFgIfBQUWcmlnaHRwYW5lIEROTkVtcHR5UGFuZWQCCQ9kFgICAQ9kFggCAQ8PFgIfAWhkZAIDDw8WAh8BaGRkAgUPZBYCAgIPFgIfAWhkAgcPZBYCAgEPZBYCAgEPZBYEAgEPZBYCZg9kFhpmD2QWAgIBDxBkEBUbA9eYMQPXmDID15gzA9eYNAPXmTED15kzA9eZNAPXmTUD15k2A9eZNwXXmdeQMQXXmdeQMwXXmdeQNAXXmdeQNQXXmdeQNgXXmdeQNwXXmdeRMQXXmdeRMwXXmdeRNQXXmdeRNgXXmdeRNwXXmdeSMQXXmdeSMgXXmdeSMwXXmdeTMQXXmdeTMgXXmdeTMxUbATEBMgEzATQBNQE2AjI2ATcBOAE5AjEwAjExAjI3AjEyAjEzAjE0AjE1AjE2AjI1AjE3AjE4AjE5AjIxAjIyAjIwAjIzAjI0FCsDG2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZxYBZmQCAg8WBB8FBQpIZWFkZXJDZWxsHwFoZAIDDxYCHwFoZAIEDxYCHwUFCkhlYWRlckNlbGxkAgYPFgQfBQUKSGVhZGVyQ2VsbB8BaGQCBw8WAh8BaGQCCA8WAh8FBQpIZWFkZXJDZWxsZAIKDxYCHwUFCkhlYWRlckNlbGxkAgwPFgIfBQUKSGVhZGVyQ2VsbGQCDg8WAh8FBQpIZWFkZXJDZWxsZAIQDxYEHwUFCkhlYWRlckNlbGwfAWhkAhEPFgIfAWhkAhIPFgQfBQUKSGVhZGVyQ2VsbB8BaGQCBQ8PFgIfAAU6157XoteV15PXm9efINecOiAyOS4wOC4yMDEzLCDXqdei15Q6IDE0OjE3LCDXnteh15o6IEExMzQwNGRkAgoPFgIfBQUVbGVmdHBhbmUgRE5ORW1wdHlQYW5lZAILDxYCHwUFF2JvdHRvbXBhbmUgRE5ORW1wdHlQYW5lZGSg2kB78p38JZ5/oQ5EYEZs/ZNJTQ==";
	}

	@Override
	public String getModuleID() {
		return "3404";
	}
	
	@Override
	public String getLessonTime(int lesonIndex){
		switch (lesonIndex) {
			case 0: return "7:25 - 8:10";
			case 1: return "8:15 - 9:00";
			case 2: return "9:00 - 9:45";
			case 3: return "10:00 - 10:45";
			case 4: return "10:45 - 11:30";
			case 5: return "11:40 - 12:25";
			case 6: return "12:55 - 13:40";
			case 7: return "13:50 - 14:35";
			case 8: return "14:35 - 15:20";
			case 9: return "15:25 - 16:05";
			case 10: return "16:05 - 16:50";
			case 11: return "17:00 - 17:45";
			case 12: return "17:45 - 18:30";
			case 13: return "18:40 - 19:25";
			case 14: return "19:25 - 20:10";
			case 15: return "20:15 - 21:00";
			case 16: return "21:00 - 21:45";
		}
		return "0";
	}

}
