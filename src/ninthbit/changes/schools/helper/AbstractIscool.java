package ninthbit.changes.schools.helper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import ninthbit.changes.ISchool;
import ninthbit.changes.SchoolClass;
import ninthbit.changes.Utils;
import ninthbit.changes.changes.Change;
import ninthbit.changes.changes.Changes;
import ninthbit.changes.timetable.Cell;
import ninthbit.changes.timetable.Day;
import ninthbit.changes.timetable.EmptyCell;
import ninthbit.changes.timetable.Lesson;

public abstract class AbstractIscool implements ISchool {

	public abstract String getURL();
	public abstract String getPostParamsChanges(String classID);
	public abstract String getPpostParamsTimeTable(String classID);
	
	@Override
	public List<SchoolClass> getClasses() throws IOException {
		List<SchoolClass> l = new ArrayList<SchoolClass>();
		Matcher matcher = Pattern.compile("<option.*?value=\"(\\d*)\".*?>(.*?)</").matcher(Utils.getRequest(getURL()));
		while (matcher.find()) {
			String classID = matcher.group(1);
			String name = matcher.group(2);
			l.add(new SchoolClass(name, classID));
		}
		return l;
	}
	
	@Override
	public List<Day> getTimeTable(String classID) throws IOException {
		
		ArrayList<Day> table = new ArrayList<Day>();
		table.add(new Day("ראשון"));
		table.add(new Day("שני"));
		table.add(new Day("שלישי"));
		table.add(new Day("רביעי"));
		table.add(new Day("חמישי"));
		table.add(new Day("שישי"));
		
		String response = Utils.postRequest(getURL(), getPpostParamsTimeTable(classID));
		Pattern pt = Pattern.compile("<table[^>]*class=\"TTTable\".*?>.*?</table>", Pattern.DOTALL);
		Matcher mt = pt.matcher(response);
		mt.find();
		
		Pattern pc = Pattern.compile("<tr.*?>.*?<td.*?>(.*?)</td>.*?<td.*?>(.*?)</td>.*?<td.*?>(.*?)</td>.*?<td.*?>(.*?)</td>.*?<td.*?>(.*?)</td>.*?<td.*?>(.*?)</td>.*?<td.*?>(.*?)</td>.*?</tr>", Pattern.DOTALL);
		Matcher mc = pc.matcher(mt.group(0));
		for(int i = -1 ; mc.find() ; i++ ){
			if( i >= 0 ){
				for(int day = 1 ; day < 7 ; day++ ){
					String html = mc.group(day+1).trim();
					ArrayList<Lesson> lessons = new ArrayList<Lesson>();
					Matcher m = Pattern.compile("<div.*?>.*?<b>(.*?)</b>.*?<br>(.*?)</div>", Pattern.DOTALL).matcher(html);
					boolean found = false;
					while(m.find()){
						found = true;
						Lesson l = new Lesson(m.group(1).trim(), m.group(2).trim());
						lessons.add(l);
					}
					if(!found) {
						table.get(day-1).addCell(new EmptyCell(i, getLessonTime(i)));
					} else {
						table.get(day-1).addCell(new Cell(i, getLessonTime(i),lessons));
					}
				}
			}
		}
		
		return table;
	}

	@Override
	public Changes getChanges(String classID) throws IOException {
		ArrayList<Change> c = new ArrayList<>();
		
		String htm = Utils.postRequest(getURL(), getPostParamsChanges(classID));
		Pattern p = Pattern.compile("<td class=\"MsgCell\" nowrap>([^<]*)</td>", Pattern.CASE_INSENSITIVE);
		Matcher m = p.matcher(htm);
		while(m.find()){
			String[] splited = m.group(1).split(",");
			String date = splited[0];
			String changeText = "";
			for (int i = 1; i < splited.length; i++) {
				changeText += " " + splited[i];
			}
			c.add(new Change(date.trim(), changeText.trim()));
		}
		
		// p = Pattern.compile("<span id=\"dnn_ctr3404_TimeTableView_lblUpdateDate\">([^<]*)</span>", Pattern.CASE_INSENSITIVE); // TODO
		p = Pattern.compile("<span id=\"dnn_ctr\\d*_TimeTableView_lblUpdateDate\">([^<]*)</span>", Pattern.CASE_INSENSITIVE); // TODO
		m = p.matcher(Utils.getRequest(getURL()));
		String lastUpdate = "";
		if(m.find())
			lastUpdate = m.group(1).replace(", שעה: ", " - ").replace("מעודכן ל: ", "").split(",")[0];
		
		return new Changes(lastUpdate, c);
	}
	
	

}
