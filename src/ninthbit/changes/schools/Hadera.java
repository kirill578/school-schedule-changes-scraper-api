package ninthbit.changes.schools;

import ninthbit.changes.schools.helper.AbstractSimpleIscool;

public class Hadera extends AbstractSimpleIscool {
	
	@Override
	public String getName() {
		return "עמל רב תחומי חדרה";
	}

	@Override
	public String getURL() {
		return "http://hadera.iscool.co.il/";
	}

	@Override
	public String getStateView() {
		return "/wEPDwUIMjU3MTQzOTcPZBYGZg8WAh4EVGV4dAU+PCFET0NUWVBFIEhUTUwgUFVCTElDICItLy9XM0MvL0RURCBIVE1MIDQuMCBUcmFuc2l0aW9uYWwvL0VOIj5kAgEPZBYMAgEPFgIeB1Zpc2libGVoZAICDxYCHgdjb250ZW50BR/Xotee15wg16jXkSDXqteX15XXnteZINeX15PXqNeUZAIDDxYCHwIFLtei157XnCDXqNeRINeq15fXldee15kg15fXk9eo15QsRG90TmV0TnVrZSxETk5kAgQPFgIfAgUg15vXnCDXlNeW15vXldeZ15XXqiDXqdee15XXqNeV16pkAgUPFgIfAgULRG90TmV0TnVrZSBkAgYPFgIfAgUf16LXntecINeo15Eg16rXl9eV157XmSDXl9eT16jXlGQCAg9kFgJmD2QWAgIED2QWAmYPZBYSAgIPZBYCZg8PFgYeCENzc0NsYXNzBQtza2luY29sdHJvbB4EXyFTQgICHwFoZGQCAw9kFgJmDw8WBh8DBQtza2luY29sdHJvbB8ABRfXm9eg15nXodeUINec157Xoteo15vXqh8EAgJkZAIFD2QWAgIBD2QWCAIBDw8WAh8BaGRkAgMPDxYCHwFoZGQCBQ9kFgICAg8WAh8BaGQCBw9kFgICAQ9kFgICAQ9kFgQCAQ9kFgJmD2QWHGYPZBYCAgEPEGQQFSIF15nXkDEF15nXkDIF15nXkDMF15nXkDQF15nXkDUF15nXkDYF15nXkDcF15nXkDgF15nXkDkG15nXkDEwBdeZ15ExBdeZ15EyBdeZ15EzBdeZ15E0BdeZ15E1BdeZ15E2BdeZ15E3BdeZ15E4BdeZ15E5BteZ15ExMAbXmdeSLTMG15nXki00BteZ15MtMwbXmdeTLTQH15nXldeTMQfXmdeV15MyB9eZ15XXkzMH15nXldeTNAfXmdeV15M1B9eZ15XXkzYH15nXldeTNwfXmdeV15M4B9eZ15XXkzkI15nXldeTMTAVIgIxMQIxMgIxMwIxNAIxNQIxNgIxNwIxOAIxOQIyMAIyMQIyMgIyMwIyNAIyNQIyNgIyNwIyOAIyOQIzMAIzMQIzMgIzMwIzNAExATIBMwE0ATUBNgE3ATgBOQIxMBQrAyJnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnFgFmZAICDxYEHgVjbGFzcwUKSGVhZGVyQ2VsbB8BaGQCAw8WAh8BaGQCBA8WAh8FBQpIZWFkZXJDZWxsZAIGDxYCHwUFCkhlYWRlckNlbGxkAggPFgIfBQUKSGVhZGVyQ2VsbGQCCg8WAh8FBQpIZWFkZXJDZWxsZAIMDxYCHwUFCkhlYWRlckNlbGxkAg4PFgIfBQUKSGVhZGVyQ2VsbGQCEA8WBB8FBQpIZWFkZXJDZWxsHwFoZAIRDxYCHwFoZAISDxYEHwUFCkhlYWRlckNlbGwfAWhkAhMPFgIfAWhkAhQPFgIfBQUKSGVhZGVyQ2VsbGQCBQ8PFgIfAAU5157XoteV15PXm9efINecOiAwNi4wOS4yMDE0LCDXqdei15Q6IDEwOjMzLCDXnteh15o6IEE4NzIxZGQCBg8WAh8FBRl0b3ByaWdodHBhbmUgRE5ORW1wdHlQYW5lZAIHDxYCHwUFGHRvcGxlZnRwYW5lIEROTkVtcHR5UGFuZWQCCA8WAh8FBRZyaWdodHBhbmUgRE5ORW1wdHlQYW5lZAIJDxYCHwUFGGNvbnRlbnRwYW5lIEROTkVtcHR5UGFuZWQCCg8WAh8FBRVsZWZ0cGFuZSBETk5FbXB0eVBhbmVkAgsPFgIfBQUXYm90dG9tcGFuZSBETk5FbXB0eVBhbmVkZKXJ2qO17c80Mo+HzmXpK69LbKIZ";
	}

	@Override
	public String getModuleID() {
		return "721";
	}

	@Override
	public String getLessonTime(int lessonIndex) {
		return "";
	}

}