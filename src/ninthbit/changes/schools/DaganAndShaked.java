package ninthbit.changes.schools;

import ninthbit.changes.schools.helper.AbstractSimpleIscool;

public class DaganAndShaked extends AbstractSimpleIscool {
	
	@Override
	public String getName() {
		return "תיכון קרית חיים";
	}

	@Override
	public String getURL() {
		return "http://tikah.iscool.co.il/Default.aspx";
	}

	@Override
	public String getStateView() {
		return "/wEPDwUIMjU3MTQzOTcPZBYGZg8WAh4EVGV4dAU+PCFET0NUWVBFIEhUTUwgUFVCTElDICItLy9XM0MvL0RURCBIVE1MIDQuMCBUcmFuc2l0aW9uYWwvL0VOIj5kAgEPZBYMAgEPFgIeB1Zpc2libGVoZAICDxYCHgdjb250ZW50BRzXqteZ15vXldefINen16jXmdeqINeX15nXmdedZAIDDxYEHwJkHwFoZAIEDxYCHwIFINeb15wg15TXlteb15XXmdeV16og16nXnteV16jXldeqZAIFDxYEHwJkHwFoZAIGDxYCHwIFMteq15nXm9eV158g16LXmdeo15XXoNeZINee16fXmdejINen16jXmdeqINeX15nXmdedZAICD2QWAmYPZBYCAgQPZBYCZg9kFhICAg9kFgJmDw8WBh4IQ3NzQ2xhc3MFC3NraW5jb2x0cm9sHgRfIVNCAgIfAWhkZAIDD2QWAmYPDxYGHwMFC3NraW5jb2x0cm9sHwAFF9eb16DXmdeh15Qg15zXntei16jXm9eqHwQCAmRkAgUPZBYCAgEPZBYIAgEPDxYCHwFoZGQCAw8PFgIfAWhkZAIFD2QWAgICDxYCHwFoZAIHD2QWAgIBD2QWAgIBD2QWBAIBD2QWAmYPZBYeZg9kFgICAQ8QZBAVOgXXk9eWMQXXk9eWMgXXk9eWMwXXk9eWNAXXk9eXMQXXk9eXMgXXk9eXMwXXk9eXNAXXk9eXNQXXk9eYMQXXk9eYMgXXk9eYMwXXk9eYNAPXmTED15kyA9eZMwPXmTQD15k1A9eZNgPXmTcD15k4A9eZOQTXmTEwBdeZ15AxBdeZ15AyBdeZ15AzBdeZ15A0BdeZ15A1BdeZ15A2BdeZ15A3BdeZ15A4BdeZ15A5BdeZ15ExBdeZ15EyBdeZ15EzBdeZ15E0BdeZ15E1BdeZ15E2BdeZ15E3BdeZ15E4BdeZ15E5Bdep15YxBdep15YyBdep15YzBdep15Y0Bdep15Y1Bdep15Y2Bdep15Y3Ddep15cxICDXqtec150F16nXlzIF16nXlzMF16nXlzQF16nXlzUM16nXmDEg16rXnNedBdep15gyBdep15gzBdep15g0Bdep15g1FToBMQEyATMBNAE1ATYBNwE4ATkCMTACMTECMTICMTMCMTUCMTYCMTcCMTgCMTkCMjACMjECMjICMjMCMjQCMjUCMjYCMjcCMjgCMjkCMzACMzECMzICMzMCMzQCMzUCMzYCMzcCMzgCMzkCNDACNDECNDICNDQCNDUCNDYCNDcCNDgCNDkCNTACNTECNTICNTMCNTQCNTUCNTYCNTcCNTgCNTkCNjAUKwM6Z2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZxYBZmQCAg8WBB4FY2xhc3MFCkhlYWRlckNlbGwfAWhkAgMPFgIfAWhkAgQPFgIfBQUKSGVhZGVyQ2VsbGQCBg8WBB8FBQpIZWFkZXJDZWxsHwFoZAIHDxYCHwFoZAIIDxYCHwUFCkhlYWRlckNlbGxkAgoPFgIfBQUKSGVhZGVyQ2VsbGQCDA8WAh8FBQpIZWFkZXJDZWxsZAIODxYCHwUFCkhlYWRlckNlbGxkAhAPFgQfBQUKSGVhZGVyQ2VsbB8BaGQCEQ8WAh8BaGQCEg8WBB8FBQpIZWFkZXJDZWxsHwFoZAITDxYCHwFoZAIUDxYEHwUFCkhlYWRlckNlbGwfAWhkAgUPDxYCHwAFOtee16LXldeT15vXnyDXnDogMDUuMDkuMjAxNCwg16nXoteUOiAwODoyMSwg157XodeaOiBBMzY3MDdkZAIGDxYCHwUFGXRvcHJpZ2h0cGFuZSBETk5FbXB0eVBhbmVkAgcPZBYCAgEPZBYGAgEPDxYCHwFoZGQCAw8PFgIfAWhkZAIFD2QWAgICDxYCHwFoZAIIDxYCHwUFFnJpZ2h0cGFuZSBETk5FbXB0eVBhbmVkAgkPFgIfBQUYY29udGVudHBhbmUgRE5ORW1wdHlQYW5lZAIKDxYCHwUFFWxlZnRwYW5lIEROTkVtcHR5UGFuZWQCCw8WAh8FBRdib3R0b21wYW5lIEROTkVtcHR5UGFuZWRk34DqJmhme3M36z0lz/tipTZ3aos=";
	}

	@Override
	public String getModuleID() {
		return "6707";
	}

	@Override
	public String getLessonTime(int lessonIndex) {
		return "";
	}

}