package ninthbit.changes.schools;

import ninthbit.changes.schools.helper.AbstractSimpleIscool;

public class Hertzog extends AbstractSimpleIscool {
	
	@Override
	public String getName() {
		return "ברנקו עש הרצוג";
	}

	@Override
	public String getURL() {
		return "http://hr.iscool.co.il/%D7%9E%D7%A2%D7%A8%D7%9B%D7%AA%D7%A9%D7%A2%D7%95%D7%AA%D7%AA%D7%A9%D7%A2%D7%93/tabid/3542/language/he-IL/Default.aspx";
	}

	@Override
	public String getStateView() {
		return "/wEPDwUIMjU3MTQzOTcPZBYGZg8WAh4EVGV4dAU+PCFET0NUWVBFIEhUTUwgUFVCTElDICItLy9XM0MvL0RURCBIVE1MIDQuMCBUcmFuc2l0aW9uYWwvL0VOIj5kAgEPZBYMAgEPFgIeB1Zpc2libGVoZAICDxYCHgdjb250ZW50BR3Xntei16jXm9eqINep16LXldeqINeq16nXoiLXk2QCAw8WAh8CBVXXkdeZ16og16HXpNeoINeq15nXm9eV158g16Ii16kg15TXqNem15XXkiwg15HXqNeg16fXlSDXldeZ15nXoSwg15HXmdeqINeX16nXnteV16DXkNeZZAIEDxYCHwIFINeb15wg15TXlteb15XXmdeV16og16nXnteV16jXldeqZAIFDxYEHwJkHwFoZAIGDxYCHwIFKdeR15nXqiDXodek16gg16rXmdeb15XXnyDXoiLXqSDXlNeo16bXldeSZAICD2QWAmYPZBYCAgQPZBYCZg9kFiICAg8WAh4FY2xhc3MFGU1lbnVTaWRlUGFuZSBETk5FbXB0eVBhbmVkAgMPFgIfAwUUVG9wUGFuZSBETk5FbXB0eVBhbmVkAgQPFgIfAwUZVG9wUmlnaHRQYW5lIEROTkVtcHR5UGFuZWQCBQ8WAh8DBRhUb3BMZWZ0UGFuZSBETk5FbXB0eVBhbmVkAgYPZBYCAgEPZBYIZg8PFgIfAWhkZAIBDw8WAh8BaGRkAgIPZBYCAgIPFgIfAWhkAgMPZBYCAgEPZBYCAgEPZBYEAgEPZBYCZg9kFh5mD2QWAgIBDxBkEBUsA9eWMQPXljID15YzA9eWNAPXljUK15Y2LSDXlyLXngPXlzED15cyA9eXMwPXlzQD15c1C9eXNiAtINeXIteeDdeYMSAtINee15Ei16gD15gyA9eYMwPXmDQT15g1LSDXntem15XXmdeg15XXqgvXmDYgLSDXlyLXng7XmDcgLSDXkNeq15LXqBTXmDggLSDXntem15XXmdeg15XXqg3XmTEgLSDXnteRIteoA9eZMgPXmTMD15k0A9eZNQvXmTYgLSDXlyLXng3XmTcgLdeQ16rXkteoFNeZOCAtINee16bXldeZ16DXldeqD9eZ15AxIC0g157XkSLXqAXXmdeQMgXXmdeQMwXXmdeQNAXXmdeQNQ3XmdeQNiAtINeXIteeENeZ15A3IC0g15DXqteS16gV15nXkDgtINee16bXldeZ16DXldeqD9eZ15ExIC0g157XkSLXqA7XmdeRMTAgLSDXlyLXngXXmdeRMwXXmdeRNAXXmdeRNRDXmdeRNyAtINeQ16rXkteoBdeZ15E4FteZ15E5IC0g157XpteV15nXoNeV16oVLAExATIBMwE0ATUBNgE3ATgBOQIxMAIxMQIxMgIxMwIxNAIxNQIxNgIxNwIxOAIxOQIyMAIyMQIyMgIyMwIyNAIyNQIyNgIyNwIyOAIyOQIzMAIzMQIzMgIzMwIzNAIzNQIzNgIzNwI0NAIzOAIzOQI0MAI0MQI0MgI0MxQrAyxnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZxYBZmQCAg8WBB8DBQpIZWFkZXJDZWxsHwFoZAIDDxYCHwFoZAIEDxYCHwMFCkhlYWRlckNlbGxkAgYPFgIfAwUKSGVhZGVyQ2VsbGQCCA8WBB8DBQpIZWFkZXJDZWxsHwFoZAIJDxYCHwFoZAIKDxYCHwMFCkhlYWRlckNlbGxkAgwPFgIfAwUKSGVhZGVyQ2VsbGQCDg8WAh8DBQpIZWFkZXJDZWxsZAIQDxYEHwMFCkhlYWRlckNlbGwfAWhkAhEPFgIfAWhkAhIPFgQfAwUKSGVhZGVyQ2VsbB8BaGQCEw8WAh8BaGQCFA8WAh8DBQpIZWFkZXJDZWxsZAIFDw8WAh8ABTvXntei15XXk9eb158g15w6IDA1LjA5LjIwMTQsINep16LXlDogMDk6MjMsINee16HXmjogQTMxMjcwN2RkAgcPFgIfAwUWUmlnaHRQYW5lIEROTkVtcHR5UGFuZWQCCA8WAh8DBRdNaWRkbGVQYW5lIEROTkVtcHR5UGFuZWQCCQ8WAh8DBRVMZWZ0UGFuZSBETk5FbXB0eVBhbmVkAgoPFgIfAwUaUmlnaHRTaWRlUGFuZSBETk5FbXB0eVBhbmVkAgsPFgIfAwUcUmlnaHRNaWRkbGVQYW5lIEROTkVtcHR5UGFuZWQCDA8WAh8DBRtMZWZ0TWlkZGxlUGFuZSBETk5FbXB0eVBhbmVkAg0PFgIfAwUZTGVmdFNpZGVQYW5lIEROTkVtcHR5UGFuZWQCDg8WAh8DBRxCb3R0b21SaWdodFBhbmUgRE5ORW1wdHlQYW5lZAIPDxYCHwMFG0JvdHRvbUxlZnRQYW5lIEROTkVtcHR5UGFuZWQCEA8WAh8DBRdCb3R0b21QYW5lIEROTkVtcHR5UGFuZWQCEg9kFgJmDw8WBh4IQ3NzQ2xhc3MFDGxvZ29uY29sdHJvbB8ABRfXm9eg15nXodeUINec157Xoteo15vXqh4EXyFTQgICZGQCEw9kFgJmDw8WBh8EBQxsb2dvbmNvbHRyb2wfBQICHwFoZGRk8jrn/bMBLJznuZKDVCitMYkApNA=";
	}

	@Override
	public String getModuleID() {
		return "12707";
	}

	@Override
	public String getLessonTime(int lessonIndex) {
		return "";
	}

}