package ninthbit.changes;

import java.io.IOException;
import java.util.List;

import ninthbit.changes.changes.Changes;
import ninthbit.changes.timetable.Day;

public interface ISchool {

	public String getName();
	public List<SchoolClass> getClasses() throws IOException;
	public Changes getChanges(String classID) throws IOException;
	public List<Day> getTimeTable(String classID) throws IOException;
	public String getLessonTime(int lessonIndex);
	
}
