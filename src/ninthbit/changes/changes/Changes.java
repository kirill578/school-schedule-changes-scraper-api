package ninthbit.changes.changes;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import ninthbit.changes.StringEscapeUtils;

import com.google.appengine.labs.repackaged.org.json.JSONArray;
import com.google.appengine.labs.repackaged.org.json.JSONException;
import com.google.appengine.labs.repackaged.org.json.JSONObject;

public class Changes {

	private String mUpdateDate;
	private ArrayList<Change> mChanges;

	public Changes(String updateTime,ArrayList<Change> changes){
		mUpdateDate = updateTime;
		mChanges = changes;
	}
	
	public String getLastUpdate(){
		return mUpdateDate;
	}
	
	public ArrayList<Change> getChanges(){
		return mChanges;
	}
	
	public String toJSON() throws JSONException, UnsupportedEncodingException{
		
		JSONArray changes = new JSONArray();
		
		for (Change change : mChanges){
			JSONArray singleChange = new JSONArray();
			singleChange.put(change.getDate());
			singleChange.put(change.getText());
			changes.put(singleChange);
		}
		
		JSONObject j = new JSONObject();
		j.accumulate("last-update", mUpdateDate);
		j.put("changes", changes);
	
		
		return j.toString();
	}

	public String toJSONold() {
		JSONArray changes = new JSONArray();
		
		for (Change change : mChanges){
			JSONArray singleChange = new JSONArray();
			singleChange.put(change.getDate());
			singleChange.put(change.getText());
			changes.put(singleChange);
		}
		
		return StringEscapeUtils.escapeJava(changes.toString());
	}
	
}
