package ninthbit.changes;

public class SchoolClass {

	private String ID;
	private String name;
	
	public SchoolClass(String name, String ID){
		this.name = name;
		this.ID = ID;
	}
	
	public String getName() {
		return this.name;
	}
	public String getID() {
		return this.ID;
	}
	
}
